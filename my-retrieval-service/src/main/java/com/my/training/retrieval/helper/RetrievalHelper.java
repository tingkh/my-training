package com.my.training.retrieval.helper;

import com.my.training.retrieval.service.RetrievalRqObject;
import com.my.training.retrieval.service.RetrievalRsObject;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.camel.language.XPath;
import org.apache.cxf.binding.soap.SoapHeader;
import org.example.retrievestudentregistration.RetrieveStudentRegistration;
import org.example.retrievestudentregistration.RetrieveStudentRegistrationResponse;
import org.example.retrievestudentregistration.StudentRegistration_Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.w3c.dom.Document;

import javax.sql.DataSource;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import java.util.ArrayList;
import java.util.List;

public class RetrievalHelper {
    private static Logger logger = LoggerFactory.getLogger(RetrievalHelper.class);

    private JdbcTemplate jdbcTemplate;

    public void setDataSource( DataSource dataSource )
    {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        //Initializing the datasource with simple query
/*		String  sqlString = "select sysdate from dual" ;
		jdbcTemplate.queryForObject(sqlString,java.lang.String.class);*/

    }

    public void retrieveStudentList (@XPath("//schoolId") String schoolId, @XPath("//registrationYear") int registrationYear)
    {

        logger.info("schoolId: {}",schoolId);
        logger.info("registrationYear: {}",registrationYear);


    }

    public  RetrieveStudentRegistrationResponse retrieveStudentRegistrationList (
            @Body RetrieveStudentRegistration retrieveStudentRegistration,
                                                                                Exchange exchange)
    {
        logger.info("SchoolId:"+retrieveStudentRegistration.getSchoolId());

        logger.info("RegistrationYear:"+retrieveStudentRegistration.getRegistrationYear());


        List<StudentRegistration_Type> returnList = new ArrayList<StudentRegistration_Type>();

        StudentRegistration_Type studentRegistration_type = new StudentRegistration_Type();
        studentRegistration_type.setStudentName("Ali");
        studentRegistration_type.setStudentAge(21);
        studentRegistration_type.setStudentNRIC("800901231432");

        returnList.add(studentRegistration_type);

        StudentRegistration_Type studentRegistration_type2 = new StudentRegistration_Type();
        studentRegistration_type2.setStudentName("Ah Kau");
        studentRegistration_type2.setStudentAge(21);
        studentRegistration_type2.setStudentNRIC("800901231433");

        returnList.add(studentRegistration_type2);

        logger.info("Returning student list");

        RetrieveStudentRegistrationResponse studentRegistrationResponse = new RetrieveStudentRegistrationResponse();
        studentRegistrationResponse.getOut().add(studentRegistration_type);
        studentRegistrationResponse.getOut().add(studentRegistration_type2);

        //exchange.setProperty("soapResponse", studentRegistrationResponse);
        return studentRegistrationResponse;

    }

    public CxfPayload<SoapHeader> getResponse(@Body String outputresponse, Exchange exchange) throws Exception
    {
        List<Source> outElements = new ArrayList<Source>();
        XmlConverter converter = new XmlConverter();

        Document outDocument = converter.toDOMDocument(outputresponse);
        outElements.add(new DOMSource(outDocument.getDocumentElement()));
        CxfPayload<SoapHeader> responsePayload = new CxfPayload<SoapHeader>(null, outElements, null);
        return responsePayload;
        //exchange.getOut().setBody(responsePayload);

    }
}
