package com.my.training.retrieval.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement

public class RetrievalStudentObject {
    @XmlElement(name = "studenNRIC")
    private String studentNRIC;

    @XmlElement(name = "registrationYear")
    private int registrationYear;

    @XmlElement(name = "studentName")
    private String studentName;

    @XmlElement(name = "studentAddress")
    private String studentAddress;

    public String getStudentNRIC() {
        return studentNRIC;
    }

    public void setStudentNRIC(String studentNRIC) {
        this.studentNRIC = studentNRIC;
    }

    public int getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(int registrationYear) {
        this.registrationYear = registrationYear;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentAddress() {
        return studentAddress;
    }

    public void setStudentAddress(String studentAddress) {
        this.studentAddress = studentAddress;
    }
}
