package com.my.training.retrieval.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement

public class RetrievalRsObject {
    @XmlElement(name = "registrationList")
    private ArrayList<RetrievalStudentObject> studentList;

    public ArrayList<RetrievalStudentObject> getStudentList() {
        return studentList;
    }

    public void setStudentList(ArrayList<RetrievalStudentObject> studentList) {
        this.studentList = studentList;
    }
}
