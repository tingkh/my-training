package com.my.training.retrieval.service;

import org.apache.cxf.jaxrs.ext.xml.XMLName;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

@WebService
public interface RetrievalServiceEndPoint {

    RetrievalRsObject retrievalService( RetrievalRqObject retrievalRqObject);
}
