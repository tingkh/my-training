
package org.example.retrievestudentregistration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studentRegistration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studentRegistration"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="studentName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="studentAge" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="studentNRIC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studentRegistration", propOrder = {
    "studentName",
    "studentAge",
    "studentNRIC"
})
public class StudentRegistration_Type {

    @XmlElement(required = true)
    protected String studentName;
    protected int studentAge;
    @XmlElement(required = true)
    protected String studentNRIC;

    /**
     * Gets the value of the studentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * Sets the value of the studentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudentName(String value) {
        this.studentName = value;
    }

    /**
     * Gets the value of the studentAge property.
     * 
     */
    public int getStudentAge() {
        return studentAge;
    }

    /**
     * Sets the value of the studentAge property.
     * 
     */
    public void setStudentAge(int value) {
        this.studentAge = value;
    }

    /**
     * Gets the value of the studentNRIC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudentNRIC() {
        return studentNRIC;
    }

    /**
     * Sets the value of the studentNRIC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudentNRIC(String value) {
        this.studentNRIC = value;
    }

}
