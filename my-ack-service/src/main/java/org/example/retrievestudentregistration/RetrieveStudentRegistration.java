
package org.example.retrievestudentregistration;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="schoolId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="registrationYear" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "schoolId",
    "registrationYear"
})
@XmlRootElement(name = "retrieveStudentRegistration")
public class RetrieveStudentRegistration {

    @XmlElement(required = true)
    protected String schoolId;
    protected int registrationYear;

    /**
     * Gets the value of the schoolId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchoolId() {
        return schoolId;
    }

    /**
     * Sets the value of the schoolId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchoolId(String value) {
        this.schoolId = value;
    }

    /**
     * Gets the value of the registrationYear property.
     * 
     */
    public int getRegistrationYear() {
        return registrationYear;
    }

    /**
     * Sets the value of the registrationYear property.
     * 
     */
    public void setRegistrationYear(int value) {
        this.registrationYear = value;
    }

}
