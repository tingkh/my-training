package com.ack.service.processLOV;

import javax.sql.DataSource;

import com.ack.datamodel.WithdrawalAckObject;
import org.apache.camel.ExchangeProperty;
import org.example.retrievestudentregistration.StudentRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ack.datamodel.LookUpRqObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

public class WithdrawalHelper {
	
	private static Logger logger = LoggerFactory.getLogger(WithdrawalHelper.class);
	private JdbcTemplate jdbcTemplate;
	StudentRegistration studentRegistration ;

	public StudentRegistration getStudentRegistration() {
		return studentRegistration;
	}

	public void setStudentRegistration(StudentRegistration studentRegistration) {
		this.studentRegistration = studentRegistration;
	}

	public void setDataSource(DataSource dataSource )
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		//Initializing the datasource with simple query
/*		String  sqlString = "select sysdate from dual" ;
		jdbcTemplate.queryForObject(sqlString,java.lang.String.class);*/

	}

	public WithdrawalAckObject getAckWithdrawalId (LookUpRqObject bodyContent)
	{
		logger.info("In function[{}] for lookupid [{}]", "getAckInquiryId", bodyContent.getInquiryId());
		String  sqlString = "select sysdate from dual" ;
		jdbcTemplate.queryForObject(sqlString,java.lang.String.class);
		WithdrawalAckObject withdrawalAckObject = new WithdrawalAckObject();
		withdrawalAckObject.setWithdrawalAckId(UUID.randomUUID().toString());
		Calendar nowCal = Calendar.getInstance();
		SimpleDateFormat simp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		withdrawalAckObject.setWithdrawalAckTimeStamp(simp.format(nowCal.getTime()));
		return withdrawalAckObject;
	}

	public java.util.List<org.example.retrievestudentregistration.StudentRegistration_Type>
		retrieveStudentList (@ExchangeProperty("registrationYear") int registrationYear,
							 @ExchangeProperty("schoolId") String schoolId)
	{
		return this.studentRegistration.retrieveStudentRegistration(schoolId,registrationYear);
	}
	

	
}



