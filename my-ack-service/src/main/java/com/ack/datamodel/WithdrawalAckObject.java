package com.ack.datamodel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WithdrawalAckObject {
    private static Logger logger = LoggerFactory.getLogger(WithdrawalAckObject.class);

    public String withdrawalAckId ;
    private String withdrawalAckTimeStamp;

    @Override
    public String toString() {
        return "WithdrawalAckObject{" +
                "withdrawalAckId='" + withdrawalAckId + '\'' +
                ", withdrawalAckTimeStamp='" + withdrawalAckTimeStamp + '\'' +
                '}';
    }

    public String getWithdrawalAckTimeStamp() {
        return withdrawalAckTimeStamp;
    }

    public void setWithdrawalAckTimeStamp(String withdrawalAckTimeStamp) {
        this.withdrawalAckTimeStamp = withdrawalAckTimeStamp;
    }

    public String getWithdrawalAckId() {

        return withdrawalAckId;
    }

    public void setWithdrawalAckId(String withdrawalAckId) {
        this.withdrawalAckId = withdrawalAckId;
    }
}
