package com.ack.datamodel;

/*@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="lookUpRqObject")*/

public class LookUpRqObject {

	private String applicationId;
	private String inquiryId;
	private String stateCd ;
	private int registrationYear ;

	public String getStateCd() {
		return stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getInquiryId() {
		return inquiryId;
	}
	public void setInquiryId(String inquiryId) {
		this.inquiryId = inquiryId;
	}

	@Override
	public String toString() {
		return "LookUpRqObject{" +
				"applicationId='" + applicationId + '\'' +
				", inquiryId='" + inquiryId + '\'' +
				", stateCd='" + stateCd + '\'' +
				", registrationYear=" + registrationYear +
				'}';
	}

	public int getRegistrationYear() {
		return registrationYear;
	}

	public void setRegistrationYear(int registrationYear) {
		this.registrationYear = registrationYear;
	}
}
