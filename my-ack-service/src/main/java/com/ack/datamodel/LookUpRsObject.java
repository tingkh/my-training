package com.ack.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="lookUpRsObject")

public class LookUpRsObject {

	private String applicationId;
	private String inquiryId;
	private String ackInquiryId;
	private String inquiryAckTimestamp;
	private String stateCd;
	private int registrationYear ;

	public String getStateCd() {
		return stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public String getInquiryAckTimestamp() {
		return inquiryAckTimestamp;
	}

	public void setInquiryAckTimestamp(String inquiryAckTimestamp) {
		this.inquiryAckTimestamp = inquiryAckTimestamp;
	}

	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getInquiryId() {
		return inquiryId;
	}
	public void setInquiryId(String inquiryId) {
		this.inquiryId = inquiryId;
	}
	public String getAckInquiryId() {
		return ackInquiryId;
	}
	public void setAckInquiryId(String ackInquiryId) {
		this.ackInquiryId = ackInquiryId;
	}

	@Override
	public String toString() {
		return "LookUpRsObject{" +
				"applicationId='" + applicationId + '\'' +
				", inquiryId='" + inquiryId + '\'' +
				", ackInquiryId='" + ackInquiryId + '\'' +
				", inquiryAckTimestamp='" + inquiryAckTimestamp + '\'' +
				", stateCd='" + stateCd + '\'' +
				", registrationYear=" + registrationYear +
				'}';
	}

	public int getRegistrationYear() {
		return registrationYear;
	}

	public void setRegistrationYear(int registrationYear) {
		this.registrationYear = registrationYear;
	}
}
