package com.ack.aggregrator;

import com.ack.datamodel.LookUpRqObject;
import com.ack.datamodel.LookUpRsObject;
import com.ack.datamodel.WithdrawalAckObject;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnrichWithdrawalIdAggregrator implements AggregationStrategy
{
    private static Logger logger = LoggerFactory.getLogger(EnrichWithdrawalIdAggregrator.class);

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        if (oldExchange!=null)
        {
            if (newExchange.getIn().getBody() instanceof WithdrawalAckObject)
            {
                WithdrawalAckObject withdrawalAckObject = (WithdrawalAckObject)newExchange.getIn().getBody();
                logger.info("In aggregator {}. Body is {}", "getAckInquiryId","withdrawalAckObject");
                LookUpRqObject lookUpRqObject = (LookUpRqObject)oldExchange.getIn().getBody();
                LookUpRsObject lookUpRsObject = new LookUpRsObject();
                lookUpRsObject.setApplicationId(lookUpRqObject.getApplicationId());
                lookUpRsObject.setInquiryId(lookUpRqObject.getInquiryId());
                lookUpRsObject.setAckInquiryId(withdrawalAckObject.getWithdrawalAckId());
                lookUpRsObject.setInquiryAckTimestamp(withdrawalAckObject.getWithdrawalAckTimeStamp());
                lookUpRsObject.setStateCd(lookUpRqObject.getStateCd());
                lookUpRsObject.setRegistrationYear(lookUpRqObject.getRegistrationYear());
                oldExchange.getIn().setBody(lookUpRsObject);
            }
            else
            {
                logger.info("In aggregator {}. Body is {}", "getAckInquiryId",newExchange.getIn().getBody().getClass().getCanonicalName());

            }
        }
        return oldExchange;
    }
}
