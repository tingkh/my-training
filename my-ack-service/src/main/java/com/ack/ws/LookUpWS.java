package com.ack.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ack.datamodel.LookUpRqObject;
import com.ack.datamodel.LookUpRsObject;


@WebService(targetNamespace = "http://ssm.org.my/service/namespace" , name = "LookUpWS")


public interface LookUpWS {
	@WebResult(name = "lookUpRsObject", targetNamespace = "http://ssm.org.my/schema/namespace")
	@WebMethod(operationName = "lookupRequest")
    public LookUpRsObject lookupRequest(
    		@WebParam(name = "lookUpRqObject", targetNamespace = "http://ssm.org.my/schema/namespace")
    		LookUpRqObject lookupRqObject);
}	