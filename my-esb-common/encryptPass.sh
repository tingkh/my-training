#encrypt database password utility
echo -n Enter Password: 
read -s password
echo \n
export mavenrepo=~/.m2/repository
java -cp ${mavenrepo}/org/jasypt/jasypt/1.9.3.redhat_3/jasypt-1.9.3.redhat_3.jar:${mavenrepo}/org/apache/camel/camel-jasypt/2.17.0.redhat-630329/camel-jasypt-2.17.0.redhat-630329.jar org.apache.camel.component.jasypt.Main -c encrypt -a PBEWithMD5AndDES -p secret -i $password
